/*global systemDictionary:true */
"use strict";

systemDictionary = {
    "ait-lwd50 adapter settings": {
        "en": "Adapter settings for ait-lwd50",
        "de": "Adaptereinstellungen für ait-lwd50",
        "ru": "Настройки адаптера для ait-lwd50",
        "pt": "Configurações do adaptador para ait-lwd50",
        "nl": "Adapterinstellingen voor ait-lwd50",
        "fr": "Paramètres d'adaptateur pour ait-lwd50",
        "it": "Impostazioni dell'adattatore per ait-lwd50",
        "es": "Ajustes del adaptador para ait-lwd50",
        "pl": "Ustawienia adaptera dla ait-lwd50",
        "zh-cn": "ait-lwd50的适配器设置"
    },
    "option1": {
        "en": "option1",
        "de": "Option 1",
        "ru": "Опция 1",
        "pt": "Opção 1",
        "nl": "Optie 1",
        "fr": "Option 1",
        "it": "opzione 1",
        "es": "Opción 1",
        "pl": "opcja 1",
        "zh-cn": "选项1"
    },
    "option2": {
        "en": "option2",
        "de": "Option 2",
        "ru": "option2",
        "pt": "opção 2",
        "nl": "Optie 2",
        "fr": "Option 2",
        "it": "opzione 2",
        "es": "opcion 2",
        "pl": "Opcja 2",
        "zh-cn": "选项2"
    }
};